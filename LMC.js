﻿//-----------------------------------------------------------------------------
// LMC.js
//-----------------------------------------------------------------------------

(function (window) {

    function LMC() {
        this.counter = new Counter();
        this.input = 0;
        this.output = 0;
        this.accumulator = new Accumulator();
        this.isRunning = false;

        this.memory = [];
        for (var i = 0; i < 100; i++) {
            this.memory[i] = 0;
        }
    };

    var p = LMC.prototype;

    p.onStop = null;
    p.onStart = null;
    p.onInput = null;
    p.onOutput = null;
    p.onCycle = null;

    p._raiseOnStop = function () {
        if (this.onStop) {
            this.onStop();
        }
    };

    p._raiseOnStart = function () {
        if (this.onStart) {
            this.onStart();
        }
    };

    p._raiseOnInput = function (pin) {
        if (this.onInput) {
            this.onInput(pin);
        }
    };

    p._raiseOnOutput = function (pin) {
        if (this.onOutput) {
            this.onOutput(pin);
        }
    };

    p._raiseOnCycle = function () {
        if (this.onCycle) {
            this.onCycle();
        }
    };

    p._log = function (msg) {
        if (this.logger) {
            this.logger.log(msg);
        }
    };

    p.run = function () {
        this._startUp();

        while (this.isRunning) {
            this.cycle();
        }   
    };

    p._startUp = function () {
        this.isRunning = true;
        this._log("> STARTED");
        this._raiseOnStart();
    };

    p.cycle = function () {

        if (!this.isRunning) {
            this._startUp();
        }

        this._raiseOnCycle();

        var instruction = decodeInstruction(this.memory[this.counter.current]);
        var address = parseInt(instruction[1]);
        var instructionCode = parseInt(instruction[0]);

        this.counter.next();

        switch (instructionCode) {
            case 0: // HLT
                this._log("HLT");
                this.stop();
                break;
            case 1: // ADD
                this._log("ADD: " + this.memory[address]);
                this.accumulator.add(this.memory[address]);
                break;
            case 2: // SUB
                this._log("SUB: " + this.memory[address]);
                this.accumulator.subtract(this.memory[address]);
                break;
            case 3: // STA
                this._log("STA: " + this.accumulator.value);
                this.memory[address] = this.accumulator.value;
                break;
            case 5: // LDA
                this._log("LDA: " + this.memory[address]);
                this.accumulator.value = this.memory[address];
                break;
            case 6: // BRA
                this._log("BRA: " + this.memory[address]);
                this.counter.set(address);
                break;
            case 7: // BRZ
                this._log("BRZ: " + this.memory[address]);
                if (this.accumulator.value == 0)
                    this.counter.set(address);
                break;
            case 8: // BRP
                this._log("BRP: " + this.memory[address]);
                if (this.accumulator.value >= 0)
                    this.counter.set(address);
                break;
            case 9: // INP/OUT
                if (address % 2 === 0) {
                    this._log("OUT: " + this.accumulator.value);
                    this.output = this.accumulator.value;
                    this._raiseOnOutput(address);
                }
                else {
                    this._raiseOnInput(address);
                    this.accumulator.value = this.input;
                    this._log("INP: " + this.accumulator.value);
                }
                break;
            default:
                break;
        }
    }

    p.stop = function stop() {
        if (this.isRunning) {
            this._log("> STOPPED");
            this.isRunning = false;
            this._raiseOnStop();
        }
    }

    p.reset = function reset() {
        this.stop();
        this.counter.reset();
        this.waitingForInput = false;
        this.input = 0;
        this.output = 0;
        this.accumulator.value = 0;
    };

    p.load = function (input) {
        var instructions = input.split("\n");
        for (var i = 0; i < instructions.length; i++) {
            this.memory[i] = parseInt(instructions[i]);
        }
    };

    function decodeInstruction(instruction) {
        var instructionCode = instruction.toString().substr(0, 1);
        var memoryLocation = instruction.toString().substr(1, 2);
        return [instructionCode, memoryLocation];
    }

    window.LMC = LMC;

    //-----------------------------------------------------------------------------
    // Accumulator
    //-----------------------------------------------------------------------------
    function Accumulator() {
        this.value = 0;

        this.add = function (value) {
            this.value += value;
        };
        
        this.subtract = function (value) {
            this.value -= value;
        };
    }

    //-----------------------------------------------------------------------------
    // Counter
    //-----------------------------------------------------------------------------
    function Counter() {
        this.current = 0;
        this.previous = 0;

        this.set = function (value) {
            this.previous = this.current;
            this.current = value;
        };

        this.next = function () {
            this.previous = this.current;
            this.current++;
        };

        this.reset = function () {
            this.current = 0;
            this.previous = 0;
        };
    }

}(window));