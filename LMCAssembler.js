﻿//-----------------------------------------------------------------------------
// LMCAssembler.js
//-----------------------------------------------------------------------------
(function (window) {
    function LMCAssembler() {
        this.labels = [];
        this.errors = [];
    }

    var p = LMCAssembler.prototype;

    // Pads the number with a leading zero
    function pad(number) {
        return (number < 10 ? '0' : '') + number
    }

    p.assemble = function (input) {
        var result = input;
        this.labels = [];
        this.errors = [];
        result = this._processInput(result);
        result = this._processLabels(result);

        return this.errors.length > 0 ? "" : result;
    }

    p._processInput = function (input) {
        var lines = input.split("\n");
        var result = "";

        for (var i = 0; i < lines.length; i++) {
            var line = this._processLine(lines[i], i);
            if (line && line.length > 0) {
                result += line + "\n";
            }
        }

        return result;
    }

    p._processLine = function (input, line) {

        // Remove the comments, leading and trailing spaces
        input = input.replace(/^\s+|\s+$|\s*\/\/.*$/g, "");

        if (input === "") {
            return;
        }

        // Set up tokenizer regex
        var tokenizer = /^\s*(?:([a-zA-Z]*)\s+)?([a-zA-Z]{3})?(?:\s+(\w+))?\s*$/;

        // Extract tokens
        var tokens = tokenizer.exec(input);
        if (tokens == null) {
            this.errors.push("Invalid syntax at line " + line);
            return;
        }
        var label = tokens[1];
        var instruction = tokens[2];
        var value = tokens[3];

        // Store the memory location for this label
        if (label != null) {
            this.labels[label] = pad(line);
        }

        result = "";

        // Translate instruction
        switch (instruction) {
            case "ADD": result += "1"; break;
            case "SUB": result += "2"; break;
            case "STA": result += "3"; break;
            case "LDA": result += "5"; break;
            case "BRA": result += "6"; break;
            case "BRZ": result += "7"; break;
            case "BRP": result += "8"; break;
            case "INP": {
                result += "9";
                if (value == null) {
                    value = "01";
                }
                break;
            }
            case "OUT": {
                result += "9";
                if (value == null) {
                    value = "02";
                }
                break;
            }
            case "HLT": result += "000"; break;
            case "DAT": break;
            default:
                this.errors.push("Unkown instruction at line " + line);
                break;
        }

        // Process value
        if (value != null) {
            result += value;
        }

        return result;
    }


    p._processLabels = function (input) {
        var result = input;
        for (var label in this.labels) {
            var len = result.length;
            result = result.replace(new RegExp(label, 'g'), this.labels[label]);
            if (len == result.length) {
                this.errors.push("Label '" + label + "' not defined.");
            }
        }
        return result;
    }

    window.LMCAssembler = LMCAssembler;

}(window));